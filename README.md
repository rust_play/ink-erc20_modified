# A simple ERC-20-Modified contract 
     
# Logic explanation
    
1. Donation can be transferred to third account, if transfer between two accounts takes place, in a range specified in the contract (min_trx_donation_threshold, max_trx_donation_threshold)
2. An account receiving donations has a donation limit. If an account receiving donations reaches a donation limit, donations are not transferred to this account any more. 
3. If donation limit is not reached by the account receiving donation and two other accounts are making transfer in a range that requires donation, donation will be calculated based on transfer value (balance)
4. Donation is calculated based on: transferred value * donation percentage (that is set for value transfer). GoTo: Erc20Modified -> donations
    
5. Test:
5a. donation_transfer_works
5b. donation_transfer_fails

